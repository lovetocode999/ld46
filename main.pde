//Images
/* @pjs preload="data/space.png, data/mars_dome.png, data/mars_terrain.png, data/tomato_plant.png, data/alien_fruit.png, data/meteor_1.png, data/meteor_2.png, data/meteor_3.png, data/meteor_4.png, data/Cabbage.png, data/Cabbage.png, data/gun_idle.png, data/gun_fire.png, data/need_alien.png, data/need_tomato.png, data/need_cabbage.png"; */ 
PImage dome;
PImage Space;
PImage ground;
PImage tomato;
PImage alien;
PImage cabbage;
PImage meteor1;
PImage meteor2;
PImage meteor3;
PImage meteor4;
PImage gun_idle;
PImage gun_fire;
PImage need_cabbage;
PImage need_tomato;
PImage need_alien;
//Other variables
boolean firing = false;
boolean isMenu = true;
boolean game = true;
boolean plant_placing = false;
int gunUpgrade = 0;
int gunCooldown = 0;
int loopNo = 0;
float difficulty = 5;
float[][] meteors = {};
boolean clicked = false;
int meteorNo = 0;
/* plant format: {x, y, type, hp, oxygen, produced, production_limit, watered_recently}
   plant types: 
 	0: tomato: 
 		oxygen: 5
		production_limit: 10
 	1: alien plant
 		oxygen: 10
		production_limit: 20
	2: cabbage
		oxygen: 5
		production_limit: 10
*/
float[][] plants = {{396, 372, 0, 100, 5, 0, 10, 20}};
int[] storage = {0, 0, 0};
int quota = -1;
int level = 0;
int money = 200;
int points = 0;
int hp = 100;
int oxygen = 100;
int damage = 0;
void setup(){
	size(640, 480);
	frameRate(30);
	noStroke();
	dome = loadImage("data/mars_dome.png");
	Space = loadImage("data/space.png");
	ground = loadImage("data/mars_terrain.png");
	tomato = loadImage("data/tomato_plant.png");
	alien = loadImage("data/alien_fruit.png");
	cabbage = loadImage("data/Cabbage.png");
	meteor1 = loadImage("data/meteor_1.png");
	meteor2 = loadImage("data/meteor_2.png");
	meteor3 = loadImage("data/meteor_3.png");
	meteor4 = loadImage("data/meteor_4.png");
	gun_fire = loadImage("data/gun_fire.png");
	gun_idle = loadImage("data/gun_idle.png");
	need_alien = loadImage("data/need_alien.png");
	need_tomato = loadImage("data/need_tomato.png");
	need_cabbage = loadImage("data/need_cabbage.png");
	dome.resize(560, 380);
	Space.resize(640, 480);
	ground.resize(640, 0);
	tomato.resize(78, 0);
	alien.resize(78, 0);
	cabbage.resize(78, 0);
	meteor1.resize(100, 0);
	meteor2.resize(100, 0);
	meteor3.resize(100, 0);
	meteor4.resize(100, 0);
	gun_fire.resize(150, 0);
	gun_idle.resize(150, 0);
	need_alien.resize(150, 0);
	need_cabbage.resize(150, 0);
	need_tomato.resize(150, 0);
}
void draw(){
	try {
		if (isMenu){
			menu();
		} else if (game) {
			background();
			image(Space, 0, 0);
			image(ground, 0, 0);
			mouseInput();
			checkCollision();
			updateMeteors();
			updateHealth(loopNo);
			drawPlants();
			if (loopNo == round(150/(difficulty-4))){
				if (level == 0) {
					level = 1;
					meteorNo = 0;
					difficulty = 5;
					genQuota();
				}
				if (level == 1) {
					genMeteor();
					meteorNo++;
					difficulty += difficulty * 0.05;
				}
				harvest();
				loopNo = 0;
			}
			textSize(20);
			text("Score: " + str(points), 20, 90);
			fill(color(0, 255, 255));
			fill(0, 200, 0);
			text("Money: " + str(money) + "$", 20, 110);
			fill(color(255, 255, 255));
			text("Tomatoes: " + str(storage[0]), 20, 130);
			fill(color(255, 255, 255));
			text("Alien Fruits: " + str(storage[1]), 20, 150);
			text("Cabbages: " + str(storage[2]), 20, 170);
			// storage quota popup
			if (quota != -1) {
				switch(quota) {
					case 0:
						image(need_tomato, 0, 300);
						break;
					case 1:
						image(need_alien, 0, 300);
						break;
					case 2:
						image(need_cabbage, 0, 300);
						break;
				}
			}
			if (plant_placing) {
				tint(255, 50);
				// tomato button
				fill(color(52, 52, 52));
				rect(410, 15, 220, 30);
				fill(color(255, 255, 255));
				textSize(19);
				text("New Tomato Plant $100", 415, 20, 210, 20);
				// alien button
				fill(color(52, 52, 52));
				rect(410, 55, 220, 30);
				fill(color(255, 255, 255));
				textSize(19);
				text("New Alien Plant $200", 415, 60, 210, 20);
				// cabbage button
				fill(color(52, 52, 52));
				rect(410, 95, 220, 30);
				fill(color(255, 255, 255));
				textSize(19);
				text("New Cabbage $100", 415, 100, 210, 20);
				// send button
				fill(color(52, 52, 52));
				rect(410, 135, 220, 30);
				fill(color(255, 255, 255));
				textSize(19);
				text("Send Plants to Colony", 415, 140, 210, 20);
				// water button
				fill(color(52, 52, 52));
				rect(410, 175, 220, 30);
				fill(color(255, 255, 255));
				textSize(19);
				text("Water all Plants", 415, 180, 210, 20);
			} else {
				text("health: " + str(hp) + "%", 20, 30);
				fill(0, 0, 255);
				text("oxygen: " + str(oxygen) + "%", 20, 50);
				fill(color(255, 0, 0));
				text("damage: " + str(damage), 20, 70);
				fill(color(135, 206, 250));
				// Fix dome button
				fill(color(42, 42, 42));
				rect(410, 15, 220, 30);
				fill(color(255, 255, 255));
				textSize(19);
				text("Fix Dome $100", 415, 20, 210, 20);
				// Fix gun button
				fill(color(42, 42, 42));
				rect(410, 55, 220, 30);
				fill(color(255, 255, 255));
				textSize(19);
				text("Upgrade Gun $100", 415, 60, 210, 20);
			}
			image(dome, 120, 80);
			if (firing) {
				image(gun_fire, 30, 300);
				firing = false;
			} else {
				image(gun_idle, 30, 300);
			}
			tint(255, 255);
			loopNo++;
			if (gunCooldown > 0) {
				gunCooldown -= 1;
			}
		} else {
			end();
		}
	} catch (e) {
		println(e);
	}
}
void menu(){
	mouseInput();
	background();
	image(Space, 0, 0);
	image(ground, 0, 0);
	textAlign(CENTER, CENTER);
	fill(color(255, 255, 255));
	textSize(70);
	text("Colonize", 0, 60, 640, 120);
	fill(color(52, 52, 52));
	rect(200, 200, 240, 80);
	fill(color(255, 255, 255));
	textSize(40);
	text("Play", 160, 200, 320, 70);
	textAlign();
}
void end(){
	background();
	image(Space, 0, 0);
	image(ground, 0, 0);
	fill(color(255, 0, 0));
	textSize(56);
	text("Game Over!", 156, 186, 320, 56);
	textSize(26);
	text("Score: " + str(points), 156, 241, 440, 26);
	textSize(21);
	text("Credits:", 156, 286, 320, 21);
	text("Code & Sound: lovetocode999 (Elijah Gregg)", 156, 316, 440, 21);
	text("Graphics: KaiExtreme~Virtualite artist and Coder", 156, 346, 480, 21);
	text("Made in 3 days for Ludum Dare 46", 156, 376, 480, 21);
}
void newPlant(x, y, type){
	if (type == 0) { // tomato
		plants = append(plants, {x, y, type, 100, 5, 0, 10, 3});
	} else if (type == 1) { // alien
		plants = append(plants, {x, y, type, 100, 10, 5, 20, 3});
	} else if (type == 2) { // cabbage
		plants = append(plants, {x, y, type, 100, 5, 0, 10, 3});
	}
}
void genMeteor(){
	float x = random(0, 640); 
	float y = 0; 
	int Image = int(random(0, 4));
	meteors = append(meteors, {x, y, Image});
	imageMode(CENTER);
	switch(Image) {
		case 0:
			image(meteor1, x, y);
			break;
		case 1:
			image(meteor2, x, y);
			break;
		case 2:
			image(meteor3, x, y);
			break;
		case 3:
			image(meteor4, x, y);
			break;
		default:
			println("Error");
	}
	imageMode(CORNER);
}
void circle(x, y, diameter){
	ellipse(x, y, diameter, diameter);
}
void updateMeteors(){
	for (int i = 0; i < meteors.length; i++) {
		float theta = atan((480-meteors[i][1])/(meteors[i][0]-320));
		meteors[i][0] = (meteors[i][0] < 320) ? meteors[i][0] + (cos(theta)*4) : meteors[i][0] - (cos(theta)*4);
		meteors[i][1] = (meteors[i][0] > 320) ? meteors[i][1] + (sin(theta)*4) : meteors[i][1] - (sin(theta)*4);
		float x = meteors[i][0];
		float y = meteors[i][1];
		int Image = meteors[i][2];
		imageMode(CENTER);
		switch(Image) {
			case 0:
				image(meteor1, x, y);
				break;
			case 1:
				image(meteor2, x, y);
				break;
			case 2:
				image(meteor3, x, y);
				break;
			case 3:
				image(meteor4, x, y);
				break;
			default:
				println("Error");
		}
		imageMode(CORNER);
	}
}
void checkCollision(){
	int[] removed = {};
	for (int i = 0; i < meteors.length; i++) {
		// check if hitting ground
		if (meteors[i][1] > 475) {
			removed = append(removed, i);
			hp -= level * int(sqrt(50));
			damage++;
		} else if (sqrt(sq(400-meteors[i][0])+sq(480-meteors[i][1])) < 290) { // check if hitting dome
			removed = append(removed, i);
			hp -= level * int(sqrt(50));
			damage++;
			playShoot();
		}

	}
	// remove unnecessary meteors to free the array of meteors
	meteors = arrayDelete(meteors, removed);
}
void arrayDelete(Array, items){
	float[] newArray = {};
	for (int i = 0; i < Array.length; i++) {
		if (! contains(i, items)) {
			newArray = append(newArray, Array[i]);
		}
	}
	return newArray;
}
void contains(item, Array){
	for (int i = 0; i < Array.length; i++) {
		if (Array[i] == item) {
			return(true);
		}
	}
	return(false);
}
void updateHealth(loopno){
	int[] removed = {};
	if (loopNo == round(150*(difficulty-4))) {
		if (damage >= 5) {
			oxygen -= damage - 4;
		}
		for (int i = 0; i < plants.length; i++) {
			oxygen += round(plants[i][4]*plants[i][3]*0.01);
			hp += round(plants[i][3]*0.01);
			if (plants[i][7] >= 1) {
				plants[i][7] -= 1;
			} else {
				removed = append(removed, i);
			}
		}
		plants = arrayDelete(plants, removed);
		hp += round(oxygen * 0.01);
		if (oxygen >= 100) {
			oxygen = 100;
		}
		if (oxygen <= 0) {
			oxygen = 0;
		}
		if (hp >= 100) {
			hp = 100;
		}
		if (hp <= 0) {
			hp = 0;
		}
		hp -= (100 - oxygen);
	}
	if (hp <= 0) {
		if (oxygen >= 100) {
			oxygen = 100;
		}
		if (oxygen <= 0) {
			oxygen = 0;
		}
		if (hp >= 100) {
			hp = 100;
		}
		if (hp <= 0) {
			hp = 0;
		}
		game = false;
	}
}
void drawPlants(){
	for (int i = 0; i < plants.length; i++) {
		if (plants[i][2] == 0) {
			image(tomato, plants[i][0], plants[i][1]);
		} else if (plants[i][2] == 1) {
			image(alien, plants[i][0], plants[i][1]);
		} else if (plants[i][2] == 2) {
			image(cabbage, plants[i][0], plants[i][1]);
		}
	}
}
void mouseInput(){
	int[] removed = {};
	if (clicked == true){
		if (sqrt(sq(320-mouseX)+sq(480-mouseY)) < 240) {
			plant_placing = (plant_placing) ? false : true;
		}
		if (plant_placing == true) {
			if (mouseX > 410 && mouseY < 45) { // tomato plant button
				if (money >= 100) {
					newPlant(random(160, 564), 372, 0);
					money -= 100;
				} else {
					jsAlert("You don't have enough money!");
				}
			} else if (mouseX > 410 && mouseY > 55 && mouseY < 85) { // alien plant button
				if (money >= 200) {
					newPlant(random(160, 564), 372, 1);
					money -= 200;
				} else {
					jsAlert("You don't have enough money!");
				}
			} else if (mouseX > 410 && mouseY > 95 && mouseY < 125) { // cabbage plant button
				if (money >= 100) {
					newPlant(random(160, 564), 372, 2);
					money -= 100;
				} else {
					jsAlert("You don't have enough money!");
				}
			} else if (mouseX > 410 && mouseY > 135 && mouseY < 165) { // send plants button
				sendPlants();
			} else if (mouseX > 410 && mouseY > 175 && mouseY < 205) { // water plants button
				for (int i = 0; i < plants.length; i++) {
					plants[i][7] = 20;
				}
			}
		} else if (isMenu) {
			if (mouseX > 200 && mouseX < 440 && mouseY > 200 && mouseY < 280) {
				isMenu = false;
				game = true;
			}
		} else if (game) {
			if (mouseX > 410 && mouseY < 45) { // fix dome button
				if (money >= 100) {
					money -= 100;
					damage -= 10;
					hp += 20;
					if (damage < 0) {
						damage = 0;
					}
					if (hp > 100) {
						hp = 100;
					}
				} else {
					jsAlert("You don't have enough money!");
				}
			} else if (mouseX > 410 && mouseY > 55 && mouseY < 85) { // upgrade gun button
				if (money >= 100) {
					money -= 100;
					gunUpgrade++;
					gunCooldown = 5;
				} else {
					jsAlert("You don't have enough money!");
				}
			}
		}
		for (int i = 0; i < meteors.length; i++) {
			if (sqrt(sq(meteors[i][0]-mouseX)+sq(meteors[i][1]-mouseY)) < 50 && gunCooldown == 0) {
				removed = append(removed, i);
				money += 5;
				points += 10;
				firing = true;
				gunCooldown = 30 - (3 * gunUpgrade);
				playShoot();
			}
		}
		meteors = arrayDelete(meteors, removed);
		clicked = false;
	}
}
void mousePressed(){
	clicked = true;
}
void harvest(){
	int[] removed = {};
	for (int i = 0; i < plants.length; i++) {
		plants[i][5]++;
		storage[plants[i][2]]++;
		if (plants[i][5] >= plants[i][6]) {
			removed = append(removed, i);
		}
	}
	plants = arrayDelete(plants, removed);
	for (int i = 0; i < storage.length; i++) {
		if (storage[i] > 20) {
			storage[i] = 20;
		}
	}
}
void sendPlants(){
	if (quota == -1) {
		jsAlert("No plants needed right now!");
	} else if (storage[quota] == 0) {
		jsAlert("You don't have the right type of harvest!");
	} else {
		points += 10 * storage[quota];
		money += 5 * storage[quota];
		storage[quota] = 0;
		level = 0;
		quota = -1;
		playEarn();
	}
}
void genQuota(){
	quota = int(random(0, 3));
}
